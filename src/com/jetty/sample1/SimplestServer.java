package com.jetty.sample1;

import org.eclipse.jetty.server.Server;

public class SimplestServer {
	public static void main(String[] args) throws Exception {
		Server server = new Server(8085);
		server.setHandler(new HelloHandlerAbstract());
		server.start();
		server.join();
	}
}