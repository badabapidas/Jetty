package com.jetty.sample2;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class BadaService {
	private String hostIP = "";
	private String contextPath = "";
	private BadaServlet badaServlet = null;
	private Server jettyServer;

	public BadaService(final String hostIP, final String contextPath) {
		this.hostIP = hostIP;
		this.contextPath = contextPath;
		badaServlet = new BadaServlet();
	}

	final void startService() throws Exception {
		init();
	}

	private void init() throws Exception {
		jettyServer = new Server();

		// Create connector for server
		final Connector connector = new SelectChannelConnector();
		connector.setPort(8787);
		connector.setHost(hostIP);

		// hook connector with server
		jettyServer.addConnector(connector);

		// Creating the resource handler
		final ResourceHandler resourceHandler = new ResourceHandler();
		resourceHandler.setWelcomeFiles(new String[] { "index.html" });

		// Setting Resource Base
		resourceHandler.setResourceBase(contextPath);

		// Enabling Directory Listing
		resourceHandler.setDirectoriesListed(true);

		final ServletContextHandler contextForVerifaya = new ServletContextHandler(ServletContextHandler.SESSIONS);
		contextForVerifaya.setContextPath("/bada");
		contextForVerifaya.addServlet(new ServletHolder(badaServlet), "/");

		final HandlerList hl = new HandlerList();
		hl.setHandlers(new Handler[] { resourceHandler, contextForVerifaya });

		jettyServer.setHandler(hl);

		jettyServer.start();
	}

	public String getHostIP() {
		return hostIP;
	}

	public void setHostIP(String hostIP) {
		this.hostIP = hostIP;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public BadaServlet getBadaServlet() {
		return badaServlet;
	}

	public void setBadaServlet(BadaServlet badaServlet) {
		this.badaServlet = badaServlet;
	}

}
