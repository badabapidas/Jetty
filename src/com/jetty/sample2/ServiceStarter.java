package com.jetty.sample2;

public class ServiceStarter {

	public static void main(String[] args) throws Exception {

		final String contexPath = "C:/SAG/SAG_Workspace/MBT_Workspace/Jetty";
		final String hostIP = "127.0.0.1";
		start(hostIP, contexPath);
	}

	private static void start(final String hostIP, final String contexPath) throws Exception {
		BadaService badaService;
		if ((hostIP.length() > 0) && (contexPath.length() > 0)) {
			badaService = new BadaService(hostIP, contexPath);
			badaService.startService();
		} else {
			System.err.println("IP or Destination Path " + hostIP + "  " + contexPath + " is missing");
			System.err.println("Service can not start");
		}
	}

}
