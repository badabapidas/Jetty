package com.jetty.sample2;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class BadaServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		System.out.println("[GET] FOUND QUERY STring ");
		if (null == req.getQueryString()) {
			resp.getOutputStream().print("Nothing found");
		} else {
			resp.getOutputStream().print(req.getQueryString());
		}

		resp.setStatus(200);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			String rawRequest = readJsonStringFromRequestBody(request);
			System.out.println("[POST]: Request Command:" + rawRequest);
			final JSONObject requestedObject = new JSONObject(rawRequest);
			final JSONObject objResponse = handleRequest(requestedObject);
			response.getOutputStream().print(objResponse.toString());
			response.setStatus(200);
		} catch (JSONException e) {
		}
	}

	private JSONObject handleRequest(JSONObject command) throws JSONException {
		JSONObject jsonResponse = command;
		try {
			final int commandName = command.getInt("badaCommand");
			switch (commandName) {
			case 0:
				System.out.println("calling command 0");
				jsonResponse.put("status", "success");
				break;
			case 1:
				System.out.println("calling command 1");
				jsonResponse.put("status", "success");
				break;
			case 2:
				System.out.println("calling command 2");
				jsonResponse.put("status", "success");
				break;

			default:
				System.out.println("calling command default");
				jsonResponse.put("status", "failed");
				jsonResponse.put("Response", "No Command found");
				break;
			}
		} catch (JSONException e) {
			jsonResponse.put("status", "failed");
			jsonResponse.put("Response", "Something went wrong. Try Again!");
		}
		return jsonResponse;
	}

	private String readJsonStringFromRequestBody(final HttpServletRequest request) {
		final StringBuffer json = new StringBuffer();
		int ch = -1;
		try {
			final BufferedReader reader = request.getReader();
			while ((ch = reader.read()) != -1) {
				final char chr = (char) ch;
				json.append(chr);
			}
		} catch (final IOException e) {
		}
		return json.toString();
	}

}
